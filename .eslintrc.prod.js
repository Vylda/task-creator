module.exports = {
  extends: ['airbnb-typescript'],
  parserOptions: {
    project: './tsconfig.dev.json',
  },
  env: {
    browser: true,
    node: true,
  },
  rules: {
    'no-console': ['error', { allow: ['warn', 'error'] }],
  },
};
