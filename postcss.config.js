const autoprefixer = require('autoprefixer');
const nestedCalcRemover = require('postcss-remove-nested-calc');

module.exports = {
  plugins: [
    nestedCalcRemover,
    autoprefixer,
  ],
};
