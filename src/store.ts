import { createStore, compose } from 'redux';
import rootReducer from './reducers/index';

declare const PRODUCTION: boolean;
let myStore: any = null;

/* eslint-disable no-underscore-dangle */
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}
if (!PRODUCTION) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  /* eslint-enable */

  myStore = createStore(rootReducer, composeEnhancers());
} else {
  myStore = createStore(rootReducer);
}
const store = myStore;
export default store;
