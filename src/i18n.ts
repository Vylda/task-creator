export type TMessages = {
  [key: string]: string | any;
};
interface Translation {
  [key: string]: TMessages;
}

const translations: Translation = {
  cs: {
    title: 'Task creator pro soubor tasks.php v Answer responderu',
    required: 'povinn{gender, select, male {ý} female {á} other {é}}',
    yes: 'Ano',
    no: 'Ne',
    close: 'Zavřít',
    tasksHeader: 'Úkoly pro jazyk {native} ({language}, {name})',
    setTranslation: 'Nastavit jazyk aplikace',
    darkMode: 'Tmavý režim',
    deleteSettings: 'Smazat nastavení aplikace',
    code: {
      header: 'Zdrojový kód pro soubor tasks.php',
      differentLengths: 'Jazykové mutace nemají stejný počet úkolů!',
      downloadPhp: 'Stáhnout PHP soubor',
    },
    copy: {
      copy: 'Kopírovat kód',
      success: 'Kód byl úspěšně zkopírován!',
      error: 'Kód se nepodařilo zkopírovat!',
    },
    task: {
      delete: 'Smazat úkol',
      add: 'Přidat úkol',
      bad: 'Tento úkol obsahuje chyby!',
      number: 'Úkol číslo {number} ({language}): {question}',
      deleteLabel: 'Doopravdy smazat úkol č. {index}',
      deleteBody: 'Úkol bude smazán ve všech jazycích!',
    },
    question: {
      label: 'Otázka',
      placeholder: 'Zadej otázku pro hráče…',
      error: 'Otázka musí být zadána!',
    },
    correctAnswers: {
      label: 'Správné odpovědi',
      placeholder: 'Zadej správnou odpověď',
      error: 'Musí být zadána alespoň jedna otázka nebo více bez přerušení!',
      answer: 'Odpověď {number}',
      errorField: 'Odpověď musí být zadána!',
      add: 'Přidat odpověď',
      delete: 'Smazat otázku',
    },
    level: {
      label: 'Úroveň hry',
      placeholder: 'Vlož úroveň hry pro úkol',
      error: 'Úroveň musí být zadána!',
    },
    badAnswers: {
      label: 'Špatné odpovědi',
      error: 'Musí být zadány všechny špatné odpověďi a nápovědné texty!',
      delete: 'Smazat špatnou otázku',
      answer: 'Špatná odpověď {number}',
      answerPlaceholder: 'Napiš špatnou odpověď',
      helper: 'Nápovědný text {number}',
      helperPlaceholder: 'Napiš nápovědný text',
      errorFieldKey: 'Špatná odpověď musí být zadána!',
      errorFieldValue: 'Nápovědný text musí být zadán!',
    },
    answerHelpers: {
      label: 'Vysvětlující texty správných odpovědí',
      text: 'Vysvětlující text ke správné odpovědi {answer}',
      placeholder: 'Zapiš vysvětlující text',
    },
    language: {
      header: 'Jazyky',
      add: 'Přidat jazyk',
      remove: 'Odstranit jazyk',
      edit: 'Upravit jazyk',
      newLabel: 'Kód jazyka podle ISO 639-1',
      newPlaceholder: 'Zadej dvoupísmenný ISO kód jazyka',
      error: 'Kód jazyka neodpovídá správnému ISO kódu nebo je už použitý!',
      noResult: 'Nic nenalezeno!',
      noSelect: 'Nic nevybráno!',
      deleteHeader: 'Skutečně smazat jazyk {language} a s ním spojené úkoly?',
      deleteBody: 'Jazyk a jeho úkoly budou nenávratně smazány!',
    },
    settings: {
      header: 'Nastavení',
      download: 'Stáhnout úkoly',
      upload: 'Nahrát úkoly',
      load: 'Nahrát',
      title: 'Nahrát soubor s úkoly',
    },
    upload: {
      label: '<b>Vyberte soubor s nastavením</b> nebo ho sem přetáhněte.',
      placeholder: 'Vyberte soubor',
      buttonText: 'Vybrat',
      errorTooMuchFiles: 'Byl nahrán více než jeden soubor!',
      errorNotJson: 'Soubor není správného typu (.json)!',
      errorReader: 'Soubor se nepodařilo načíst!',
      errorNotValidJSON: 'Soubor s úkoly nemá správný formát dat!',
      errorBadKeys: 'Jazyky nemají správné názvy (podle ISO 639-1)!',
      errorNotArray: '{langNum, plural, =1 {Jazyk} other {Jazyky}} {languages} {langNum, plural, =1 {nemá} other {nemají}} správně zapsaná data!',
      errorTasksNotCorrect: 'Alespoň jeden úkol v {langNum, plural, =1 {jazyce} other {jazycích}} {languages} nemá správný formát!',
    },
    help: {
      help: 'Nápověda',
      question: '<b>Otázka</b>, která se hráči pošle na jeho e-mail (<b>vyjma první otázky</b>, protože tu se hráč dozví jinde).',
      correctAnswers: '<b>Jedna nebo více</b> správných odpovědí; nebere se ohled na malá a velká písmenka; mezery na začátku a na konci se oříznou a mezi slovy se zkrátí na jednu; lze použít i písmena s diakritikami.',
      level: '<b>Celé kladné číslo</b>, které určuje úroveň hry, pro kterou je otázka určena. Pokud <b>nemají být</b> úlohy předkládány podle <b>aktuální úrovně</b> hry nebo <b>nejsou</b> úkoly předkládány <b>náhodně</b>, může být zapsáno <b>libovolné číslo</b>. <b>První úkol</b> musí mít <b>vždy úroveň 0</b>!',
      badAnswers: '<b>Nepovinné</b>. Jedna nebo více špatných odpovědí, po jejichž zadání uvidí hráč odpovídající nápovědný text. Pokud je použiješ, musíš zadat jak špatnou odpověď, tak i její nápovědný text. Pro špatné odpovědi platí stejná pravidla jako pro správné odpovědi.',
      helpers: '<b>Nepovinné</b>. K jednotlivým správným otázkám můžeš zadat doprovodné (vysvětlující) texty, které se se objeví ve webové aplikaci. Pokud napíšeš %answer%, tak se nahradí odpovědí hráče, znak \\n vytvoří zalomení řádku.',
      language: 'Hra může mít více jazykových mutací, které se nastavují zde.',
      settings: 'Pomocí této sekce si data můžeš uložit do souboru anebo je zpět nahrát pro jejich úpravu.',
    },
  },
  en: {
    title: 'Task creator for tasks.php in Answer responder',
    required: 'required',
    yes: 'Yes',
    no: 'No',
    close: 'Close',
    tasksHeader: 'Tasks for {native} ({language}, {name})',
    setTranslation: 'Set application language',
    darkMode: 'Dark mode',
    deleteSettings: 'Delete app settings',
    code: {
      header: 'Source code for tasks.php file',
      differentLengths: 'Language mutations do not have the same number of tasks!',
      downloadPhp: 'Download PHP file',
    },
    copy: {
      copy: 'Copy code',
      success: 'Code copied successfully!',
      error: 'Failed to copy code!',
    },
    task: {
      delete: 'Delete task',
      add: 'Add Task',
      bad: 'This task has errors!',
      number: 'Task # {number} ({language}): {question}',
      deleteLabel: 'You are sure to delete task # {index}',
      deleteBody: 'The task will be deleted in all languages!',
    },
    question: {
      label: 'Question',
      placeholder: 'Type question for player…',
      error: 'Question must be inserted!',
    },
    correctAnswers: {
      label: 'Correct answers',
      placeholder: 'Type correct answer',
      error: 'At least one or more questions must be entered without interruption!',
      answer: 'Answer {number}',
      errorField: 'The answer must be entered!',
      add: 'Add answer',
      delete: 'Remove answer',
    },
    level: {
      label: 'Game level',
      placeholder: 'Enter the game level for the task',
      error: 'Level must be entered!',
    },
    badAnswers: {
      label: 'Wrong answers',
      error: 'All wrong answers and help texts must be entered!',
      delete: 'Delete wrong answer',
      answer: 'Wrong answer # {number}',
      answerPlaceholder: 'Type wrong answer',
      helper: 'Help text {number}',
      helperPlaceholder: 'Type help text',
      errorFieldKey: 'Wrong answer must be entered!',
      errorFieldValue: 'Help text mus be entered!',
    },
    answerHelpers: {
      label: 'Explanatory texts of correct answers',
      text: 'Explanatory text for correct answer {answer}',
      placeholder: 'Type explanatory text',
    },
    language: {
      header: 'Languages',
      add: 'Add languages',
      remove: 'Remove languages',
      edit: 'Edit language',
      newLabel: 'ISO 639-1 language code',
      newPlaceholder: 'Enter the two-letter ISO language code',
      error: 'The language code does not match the correct ISO code or is already in use!',
      noResult: 'Nothing found!',
      noSelect: 'Nothing selected!',
      deleteHeader: 'Are you sure you want to delete {language} and its associated tasks?',
      deleteBody: 'The language and its tasks will be permanently deleted!',
    },
    settings: {
      header: 'Settings',
      download: 'Download tasks',
      upload: 'Upload tasks',
      load: 'Load',
      title: 'Load file with tasks',
    },
    upload: {
      label: '<b>Choose a configuration file</b> or move it here.',
      placeholder: 'Select file',
      buttonText: 'Select',
      errorTooMuchFiles: 'More than one file uploaded!',
      errorNotJson: 'The file is not the correct type (.json)!',
      errorReader: 'Failed to load file!',
      errorNotValidJSON: 'The task file does not have the correct data format!',
      errorBadKeys: 'The languages do not have the correct names (according to ISO 639-1)!',
      errorNotArray: 'The {langNum, plural, =1 {language} other {languages}} {languages} does not have the data written correctly!',
      errorTasksNotCorrect: 'At least one task in {langNum, plural, =1 {language} other {languages}} {languages} is not in the correct format!',
    },
    help: {
      help: 'Help',
      question: '<b>Question</b> that will be sent to the player\'s email(<b>except for the first question</b>, because the player will find out elsewhere).',
      correctAnswers: '<b>One or more</b> correct answers; case insensitive; spaces at the beginning and end are truncated and shortened between words to one; letters with accents can also be used.',
      level: '<b>A positive integer</b> that identifies the level of the game for which the question is intended. If <b>tasks</b> are not to be submitted according to the <b>current level</b> of the game or <b>tasks</b> are not submitted <b>randomly</b>, <b>any number</b> can be entered. <b>The first task</b> must <b>always have level 0</b>!',
      badAnswers: '<b>Optional</b>. One or more incorrect answers, after which the player will see the corresponding help text. If you use them, you must enter both the wrong answer and its help text. The same rules apply to incorrect answers as to correct answers.',
      helpers: '<b>Optional</b>. You can enter accompanying (explanatory) texts for each correct question, which will appear in the web application. If you type %answer%, it will be replaced by the player\'s answer, the \\n will create a line break.',
      language: 'The game can have multiple language versions, which are set here.',
      settings: 'Using this section, you can save the data to a file or upload it back for editing.',
    },
  },
};

export default translations;
