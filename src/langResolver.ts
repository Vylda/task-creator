import { createIntl, createIntlCache } from 'react-intl';
import translations, { TMessages } from './i18n';

const tableize = require('tableize-object');

/**
 * Test, if object is empty
 * @param obj
 */
const emptyObject = (obj: any) => {
  const keysLength = Object.keys(obj).length;
  return keysLength === 0 && obj.constructor === Object;
};

/**
 * Get translation object
 * @returns object {langCode, translations, intl function}
 */
export const getTranslations = (): {
  langCode: string;
  messages: TMessages;
  intl: any;
} => {
  let langCode: string = localStorage.getItem('defaultLanguage') || navigator.language.toLowerCase();

  function getProperty<T, K extends keyof T>(o: T, property: K): T[K] {
    return o[property];
  }

  let messages: TMessages = tableize(getProperty(translations, langCode));

  if (emptyObject(messages)) {
    langCode = langCode.slice(0, 2);
    messages = tableize(getProperty(translations, langCode));

    if (emptyObject(messages)) {
      langCode = 'cs';
      messages = tableize(translations.cs);
    }
  }

  const cache = createIntlCache();
  const intl = createIntl({
    locale: langCode.toString(),
    messages,
  }, cache);

  return { langCode: langCode.toString(), messages, intl };
};

export const { langCode, messages, intl } = getTranslations();

export const languageMutations = (): string[] => Object.keys(translations);
