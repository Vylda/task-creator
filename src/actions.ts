import { IconName } from '@blueprintjs/core';
import { ReactNode } from 'react';
import {
  CHANGE_OPEN_TASK_INDEX, ADD_TASK, CHANGE_TASK, REMOVE_TASK,
  ADD_NEW_LANGUAGE, SET_LANGUAGE, CHANGE_LANGUAGE_NAME, REMOVE_LANGUAGE,
  SET_DIALOG_OPEN, SET_DIALOG_DONE, ADD_TASKS, CHANGE_TASKS_LANGUAGE,
  REMOVE_TASKS_LANGUAGE, SET_TASKS, SET_ALL_LANGUAGES,
} from './constants';
import { ITask, ITasksObject } from './reducers/tasks';
import { TLanguage } from './reducers/language';

export const setOpenedTaskIndexAction = (index: number) => ({
  type: CHANGE_OPEN_TASK_INDEX,
  index,
});

export const addTaskAction = (newIndex: number, language: string) => ({
  type: ADD_TASK,
  newIndex,
  language,
});

export const addTasksAction = (tasks: ITask[], language: string) => ({
  type: ADD_TASKS,
  tasks,
  language,
});

export const removeTaskAction = (index: number) => ({
  type: REMOVE_TASK,
  index,
});

export const changeTaskAction = (task: ITask, index: number, language: string) => ({
  type: CHANGE_TASK,
  task,
  index,
  language,
});

export const addNewLanguageAction = (languageCode: string) => ({
  type: ADD_NEW_LANGUAGE,
  languageCode,
});

export const editLanguageAction = (languageCode: string, oldLanguageCode: string) => ({
  type: CHANGE_LANGUAGE_NAME,
  languageCode,
  oldLanguageCode,
});

export const removeLanguageAction = (languageCode: string) => ({
  type: REMOVE_LANGUAGE,
  languageCode,
});

export const removeLanguageTasksAction = (languageCode: string) => ({
  type: REMOVE_TASKS_LANGUAGE,
  languageCode,
});

export const setCurrentLanguageAction = (languageCode: string) => ({
  type: SET_LANGUAGE,
  languageCode,
});

export const changeTasksLanguageAction = (languageCode: string, oldLanguageCode: string) => ({
  type: CHANGE_TASKS_LANGUAGE,
  languageCode,
  oldLanguageCode,
});

export const setDialogAction = (
  dialogIsOpen: boolean,
  title: string = '',
  body: ReactNode = '',
  done: boolean = false,
  icon: IconName = 'warning-sign',
) => ({
  type: SET_DIALOG_OPEN,
  dialog: {
    dialogIsOpen,
    done,
    icon,
    title,
    body,
  },
});

export const setDialogDoneAction = (
  done: boolean,
) => ({
  type: SET_DIALOG_DONE,
  done,
});

export const setAllTasksAction = (tasks: ITasksObject) => ({
  type: SET_TASKS,
  tasks,
});

export const setAllLanguagesAction = (languageState: TLanguage) => ({
  type: SET_ALL_LANGUAGES,
  languageState,
});
