import React from 'react';
import { ItemPredicate } from '@blueprintjs/select';

const escapeRegExpChars = (text: string) => text.replace(/([.*+?^=!:${}()|[\]/\\])/g, '$1');

export const highlightText = (text: string, query: string) => {
  let lastIndex = 0;
  const words = query
    .split(/\s+/)
    .filter((word) => word.length > 0)
    .map(escapeRegExpChars);

  if (words.length === 0) {
    return [text];
  }

  const regexp = new RegExp(words.join('|'), 'gi');
  const tokens: React.ReactNode[] = [];
  let isMatch = true;
  while (isMatch) {
    const match = regexp.exec(text);
    if (!match) {
      isMatch = false;
      break;
    }

    const { length } = match[0];
    const before = text.slice(lastIndex, regexp.lastIndex - length);

    if (before.length > 0) {
      tokens.push(before);
    }

    lastIndex = regexp.lastIndex;
    tokens.push(
      <strong key={lastIndex}>
        {match[0]}
      </strong>,
    );
  }

  const rest = text.slice(lastIndex);
  if (rest.length > 0) {
    tokens.push(rest);
  }
  return tokens;
};

export const filterItems: ItemPredicate<string> = (query, item) => {
  const normalizedItem = item.toLowerCase();
  const normalizedQuery = query.toLowerCase();

  return normalizedItem.indexOf(normalizedQuery) >= 0;
};
