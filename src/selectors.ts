import { createSelector } from 'reselect';

const getOpenedTaskIndexSelector = (state: any) => state.openedTaskIndex;
const tasksSelector = (state: any) => state.tasks;
const languageSelector = (state: any) => state.language;
const dialogSelector = (state: any) => state.dialog;

export const getTasks = createSelector(
  tasksSelector,
  (tasks) => tasks,
);

export const getOpenedTaskIndex = createSelector(
  getOpenedTaskIndexSelector,
  (openedTaskIndex) => openedTaskIndex,
);

export const getOpenTask = createSelector(
  [tasksSelector, getOpenedTaskIndexSelector],
  (tasks, index) => tasks[Math.min(tasks.length - 1, index)],
);

export const getCurrentLanguage = createSelector(
  languageSelector,
  (language) => language.current,
);

export const getLanguages = createSelector(
  languageSelector,
  (language) => language.languages,
);

export const getDialogIsOpen = createSelector(
  dialogSelector,
  (dialog) => dialog.dialogIsOpen,
);

export const getDialogDone = createSelector(
  dialogSelector,
  (dialog) => dialog.done,
);

export const getDialogIcon = createSelector(
  dialogSelector,
  (dialog) => dialog.icon,
);

export const getDialogTitle = createSelector(
  dialogSelector,
  (dialog) => dialog.title,
);

export const getDialogBody = createSelector(
  dialogSelector,
  (dialog) => dialog.body,
);
