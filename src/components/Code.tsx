import React, { useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  Button, Intent, Toaster, Position, Card, Icon,
} from '@blueprintjs/core';
import fileDownload from 'js-file-download';
import { getTasks } from '../selectors';
import { ITask, ITasksObject, TBadAnswer } from '../reducers/tasks';

type TCode = {
  tasks: ITasksObject;
};

const Code = ({
  tasks,
}: TCode) => {
  const [tasksMainCode, setTaskMainCode] = useState('');
  const [hasDifferentLengths, setHasDifferentLength] = useState(false);

  const toaster = useRef(null);
  const { formatMessage } = useIntl();
  const SPACES = 8;

  const createSimpleTexts = (texts: string[]): string => texts
    .map((ca) => `"${ca}"`)
    .join(`,\n${' '.repeat(SPACES)}`);

  const createBadAnswersText = (answers: TBadAnswer[]): string => {
    const filteredAnswers: TBadAnswer[] = answers.filter(
      (answer: TBadAnswer) => answer.key && answer.value,
    );
    const rows: string[] = filteredAnswers.map((answer) => `"${answer.key}" => "${answer.value}"`);
    return rows.join(`,\n${' '.repeat(SPACES)}`);
  };

  const createOptionals = (badAnswers: TBadAnswer[], helpers: string[]): string => {
    const badAnswersText = createBadAnswersText(badAnswers);
    const helperTexts = createSimpleTexts(helpers);

    let finalText = '';

    if (badAnswersText) {
      finalText = `,
      [
        ${badAnswersText}
      ]`;
    }

    if (helperTexts) {
      if (!finalText) {
        finalText = `,
      []`;
      }
      finalText = `${finalText},
      [
        ${helperTexts}
      ]`;
    }

    return finalText;
  };

  const createTaskCode = (tasksTextArray: string[], language: string): string => {
    const filteredArray = tasksTextArray.filter((row) => row);
    const taskText = filteredArray.length && filteredArray
      ? tasksTextArray.join(',\n')
      : null;

    const text = taskText
      ? `  "${language}" => [\n${taskText}
  ]`
      : '';

    return text;
  };

  const copyHandler = (txt: string) => {
    const timeout = 3000;
    try {
      navigator.clipboard.writeText(txt).then(() => {
        toaster.current.show({
          message: formatMessage({ id: 'copy.success' }),
          intent: Intent.SUCCESS,
          icon: 'tick',
          timeout,
        });
      }, () => {
        toaster.current.show({
          message: formatMessage({ id: 'copy.error' }),
          intent: Intent.DANGER,
          icon: 'error',
          timeout,
        });
      });
    } catch (err) {
      toaster.current.show({
        message: formatMessage({ id: 'copyError' }),
        intent: Intent.DANGER,
        icon: 'error',
        timeout,
      });
    }
  };

  const downloadCodeHandler = (codeText: string) => {
    const code = `<?php\n${codeText}`;
    fileDownload(code, 'tasks.php');
  };

  useEffect(() => {
    const langKeys: string[] = Object.keys(tasks);
    const mainLangCodes: string[] = [];
    const lengths: number[] = [];
    for (let index: number = 0; index < langKeys.length; index += 1) {
      const language = langKeys[index];
      const tasksMap: string[] = tasks[language]
        .filter((task: ITask) => task.valid)
        .map(
          (task: ITask) => {
            const answers: string = createSimpleTexts(task.correctAnswers
              .filter((answer) => answer.trim()));
            const optionals: string = createOptionals(task.badAnswers, task.helpers);

            const taskCode = `    new Task (
      "${task.question}",
      [
        ${answers}
      ],
      ${task.level}${optionals}
    )`;
            return taskCode;
          },
        );
      lengths.push(tasksMap.length);
      const mainLangCode: string = createTaskCode(tasksMap, language);
      if (mainLangCode) {
        mainLangCodes.push(mainLangCode);
      }
    }

    setHasDifferentLength((new Set(lengths)).size > 1);

    setTaskMainCode(mainLangCodes.length
      ? `$gameTasks = [
${mainLangCodes.join(',\n')}
];`
      : '$gameTasks = [];');
  }, [tasks]);

  return (
    <>
      <h2>
        <FormattedMessage id="code.header" />
      </h2>
      <div className="code-button-line">
        <Button
          icon="clipboard"
          onClick={() => copyHandler(tasksMainCode)}
          intent={Intent.PRIMARY}
        >
          <FormattedMessage id="copy.copy" />
        </Button>
        <Button
          icon="code"
          onClick={() => downloadCodeHandler(tasksMainCode)}
          intent={Intent.NONE}
        >
          <FormattedMessage id="code.downloadPhp" />
        </Button>
      </div>
      <div className="code-wrapper">
        <div className="source-code">
          {tasksMainCode}
        </div>
      </div>
      <Toaster
        ref={toaster}
        position={Position.TOP}
        className="code-toaster"
      />
      {hasDifferentLengths && (
        <div id="different-lengths">
          <Card elevation={3}>
            <Icon icon="warning-sign" iconSize={36} intent={Intent.WARNING} />
            <span>
              <FormattedMessage id="code.differentLengths" />
            </span>
          </Card>
        </div>
      )}
    </>
  );
};

const mapStateToProps = (state: any) => ({
  tasks: getTasks(state),
});

export default connect(mapStateToProps)(Code);
