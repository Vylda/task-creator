import React from 'react';
import { FormattedMessage } from 'react-intl';

const Header = () => (
  <h1 className="bp3-heading">
    <FormattedMessage id="title" />
  </h1>
);

export default Header;
