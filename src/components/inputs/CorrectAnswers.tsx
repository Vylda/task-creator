import React, { useEffect, useState, useRef } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  Card, FormGroup, InputGroup, Intent, Button, Tooltip,
} from '@blueprintjs/core';
import Help from '../Help';

interface IAnswers {
  index: number;
  answers: string[];
  onChange?: Function;
  onValidate?: Function;
}

const CorrectAnswers = ({
  index, onChange, onValidate, answers,
}: IAnswers) => {
  const intl = useIntl();
  const [isValid, setIsValid] = useState(false);
  const [answerFields, setAnswerFields] = useState([]);
  const [stateAnswers, setStateAnswers] = useState(answers);

  const lastInput = useRef(null);

  const validate = (valuesToTest: string[]): void => {
    const noEmpty: boolean = valuesToTest.some((valueToTest) => !!valueToTest);

    let hasNoEmptyBeforeLast: boolean = true;
    if (valuesToTest.length > 1) {
      const withoutLast = [...valuesToTest];
      withoutLast.pop();
      hasNoEmptyBeforeLast = !withoutLast.some((valueToTest) => !valueToTest);
    }

    const valid = noEmpty && hasNoEmptyBeforeLast;
    setIsValid(valid);

    if (onValidate) {
      onValidate(valid);
    }
  };

  const onChangeHandler = (
    evt: React.FormEvent<HTMLInputElement>,
    aIndex: number,
  ) => {
    const newValue = evt.currentTarget.value.trim();
    const tempAnswers = [...stateAnswers];

    tempAnswers[aIndex] = newValue;
    setStateAnswers(tempAnswers);
  };

  const addHandler = () => {
    const newAnswers = [...stateAnswers];
    newAnswers.push('');
    setStateAnswers(newAnswers);
  };

  const deleteHandler = (answerIndex: number) => {
    const newAnswers = stateAnswers.filter((answer, aIndex) => aIndex !== answerIndex);
    setStateAnswers(newAnswers);
  };

  const blurHandler = () => {
    lastInput.current = null;
  };

  const makeAnswers = (myAnswers: string[]) => {
    const answersToMake = myAnswers;
    if (!answersToMake.length) {
      answersToMake.push('');
    }

    let answerKey = -1;
    const answFields = answersToMake.map((answer, aIndex) => {
      answerKey += answerKey;

      const validField: boolean = aIndex === answersToMake.length - 1 && aIndex !== 0
        ? true
        : !!answer;

      const deleteButton = (
        <Tooltip content={intl.formatMessage({ id: 'correctAnswers.delete' })}>
          <Button
            icon="delete"
            intent={Intent.WARNING}
            minimal
            onClick={() => { deleteHandler(aIndex); }}
          />
        </Tooltip>
      );

      return (
        <FormGroup
          helperText={validField ? '' : intl.formatMessage({ id: 'correctAnswers.errorField' })}
          label={intl.formatMessage({ id: 'correctAnswers.answer' }, { number: aIndex + 1 })}
          labelFor={`answer-input-${index}-${answerKey}`}
          labelInfo={aIndex ? '' : `(${intl.formatMessage({ id: 'required' }, { gender: 'female' })})`}
          intent={isValid ? Intent.SUCCESS : Intent.DANGER}
          key={`answer-input-${index}-${answerKey}`}
        >
          <InputGroup
            id={`answer-input-${index}-${answerKey}`}
            placeholder={intl.formatMessage({ id: 'correctAnswers.placeholder' })}
            value={answer}
            intent={answer ? Intent.SUCCESS : Intent.DANGER}
            onChange={(evt: React.FormEvent<HTMLInputElement>) => {
              onChangeHandler(evt, aIndex);
            }}
            leftIcon="clean"
            rightElement={aIndex === 0 ? null : deleteButton}
            inputRef={answer ? null : lastInput}
            onBlur={answer ? null : blurHandler}
          />
        </FormGroup>
      );
    });

    setAnswerFields(answFields);
  };

  useEffect(() => {
    makeAnswers(stateAnswers);

    if (onChange) {
      const tempAnswers = [...stateAnswers];
      onChange(tempAnswers);
      validate(tempAnswers);
    }
  }, [stateAnswers]);

  useEffect(() => {
    if (lastInput.current) {
      lastInput.current.focus();
    }
  }, [answerFields]);

  const help = (
    <FormattedMessage
      id="help.correctAnswers"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    <div className="field-group">
      <FormGroup
        helperText={isValid ? '' : intl.formatMessage({ id: 'correctAnswers.error' })}
        label={intl.formatMessage({ id: 'correctAnswers.label' })}
        labelInfo={`(${intl.formatMessage({ id: 'required' }, { gender: 'other' })})`}
        intent={isValid ? Intent.SUCCESS : Intent.DANGER}
      >
        <Card className={isValid ? '' : 'invalid'}>
          {answerFields}
          <Button
            icon="add"
            intent={Intent.SUCCESS}
            title={intl.formatMessage({ id: 'correctAnswers.add' })}
            onClick={addHandler}
          />
        </Card>
      </FormGroup>
      <Help>{help}</Help>
    </div>
  );
};

CorrectAnswers.defaultProps = {
  onChange: null,
  onValidate: null,
};
export default CorrectAnswers;
