import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  Card, FormGroup, InputGroup, Intent,
} from '@blueprintjs/core';
import Help from '../Help';

interface IAnswerHelpers {
  helpers: string[];
  answers: string[];
  index: number;
  onChange?: Function;
}

const AnswerHelpers = ({
  helpers, answers, index, onChange,
}: IAnswerHelpers) => {
  const intl = useIntl();
  const [textFields, setTextFields] = useState([]);
  const [stateTexts, setStateTexts] = useState(helpers);

  const onChangeHandler = (
    evt: React.FormEvent<HTMLInputElement>,
    tIndex: number,
  ) => {
    const newValue = evt.currentTarget.value.trim();
    const tempTexts = [...stateTexts];

    tempTexts[tIndex] = newValue || '';
    setStateTexts(tempTexts);
  };

  const makeTexts = (myAnswers: string[], myTexts: string[]) => {
    let textKey = -1;
    const txtFields = myAnswers.map((answer, aIndex) => {
      if (answer) {
        textKey += textKey;

        return (
          <FormGroup
            label={intl.formatMessage({ id: 'answerHelpers.text' }, { answer })}
            labelFor={`answer-helpers-input-${index}-${textKey}`}
            key={`answer-helpers-input-${index}-${textKey}`}
            intent={Intent.SUCCESS}
            className="answer-helper-box"
          >
            <InputGroup
              id={`answer-helpers-input-${index}-${textKey}`}
              placeholder={intl.formatMessage({ id: 'answerHelpers.placeholder' })}
              value={myTexts[aIndex] || ''}
              intent={Intent.SUCCESS}
              onChange={(evt: React.FormEvent<HTMLInputElement>) => {
                onChangeHandler(evt, aIndex);
              }}
              leftIcon="clean"
            />
          </FormGroup>
        );
      }
      return null;
    });
    setTextFields(txtFields);
  };

  useEffect(() => {
    makeTexts(answers, stateTexts);
  }, [answers]);

  useEffect(() => {
    makeTexts(answers, stateTexts);

    if (onChange) {
      const tempTexts = [...stateTexts];
      while (tempTexts.length > 0 && !tempTexts[tempTexts.length - 1]) {
        tempTexts.pop();
      }
      const resultTexts = tempTexts.map((text) => text || '');
      onChange(resultTexts);
    }
  }, [stateTexts]);

  const help = (
    <FormattedMessage
      id="help.helpers"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    <div className="field-group">
      <FormGroup
        label={intl.formatMessage({ id: 'answerHelpers.label' })}
        intent={Intent.SUCCESS}
      >
        <Card>
          {textFields}
        </Card>
      </FormGroup>
      <Help>{help}</Help>
    </div>
  );
};

AnswerHelpers.defaultProps = {
  onChange: null,
};
export default AnswerHelpers;
