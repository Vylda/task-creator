import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { FormGroup, NumericInput, Intent } from '@blueprintjs/core';
import Help from '../Help';

interface ILevelProps {
  value: number;
  index: number;
  onChange?: Function;
  onValidate?: Function;
  disabled: boolean;
}

const Level = ({
  value, index, onChange, onValidate, disabled,
}: ILevelProps) => {
  const intl = useIntl();
  const [currentValue, setCurrentValue] = useState(value);
  const [isValid, setIsValid] = useState(false);

  const validate = (valueToTest: number): void => {
    let valid: boolean = true;
    if (!Number.isInteger(valueToTest)) {
      valid = false;
    }
    if (valueToTest < 0) {
      valid = false;
    }

    setIsValid(valid);

    if (onValidate) {
      onValidate(valid);
    }
  };

  useEffect(() => {
    validate(value);
  }, [value]);

  const valueChangeHandler = (valueAsNumber: number): void => {
    setCurrentValue(valueAsNumber);
    validate(valueAsNumber);

    if (onChange) {
      onChange(valueAsNumber);
    }
  };

  const help = (
    <FormattedMessage
      id="help.level"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    <div className="field-group">
      <FormGroup
        helperText={isValid ? '' : intl.formatMessage({ id: 'level.error' })}
        label={intl.formatMessage({ id: 'level.label' })}
        labelFor={`level-input-${index}`}
        labelInfo={`(${intl.formatMessage({ id: 'required' }, { gender: 'female' })})`}
        intent={isValid ? Intent.SUCCESS : Intent.DANGER}
        disabled={disabled}
      >
        <NumericInput
          id={`level-input-${index}`}
          placeholder={intl.formatMessage({ id: 'level.placeholder' })}
          value={currentValue}
          intent={isValid ? Intent.SUCCESS : Intent.DANGER}
          onValueChange={!disabled && valueChangeHandler}
          leftIcon="layers"
          stepSize={1}
          min={0}
          minorStepSize={null}
          fill
        />
      </FormGroup>
      <Help>{help}</Help>
    </div>
  );
};

Level.defaultProps = {
  onChange: null,
  onValidate: null,
};
export default Level;
