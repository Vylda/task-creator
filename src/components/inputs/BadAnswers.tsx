import React, { useEffect, useState, useRef } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  Card, FormGroup, InputGroup, Intent, Button, Tooltip,
} from '@blueprintjs/core';
import { TBadAnswer } from '../../reducers/tasks';
import Help from '../Help';

interface IBadAnswers {
  badAnswers: TBadAnswer[];
  index: number;
  onChange?: Function;
  onValidate?: Function;
}

const BadAnswers = ({
  badAnswers, index, onChange, onValidate,
}: IBadAnswers) => {
  const intl = useIntl();
  const [isValid, setIsValid] = useState(false);
  const [answerFields, setAnswerFields] = useState([]);
  const [stateBadAnswers, setStateBadAnswers] = useState([]);

  const lastInput = useRef(null);

  const validate = (valuesToTest: TBadAnswer[]): void => {
    if (!valuesToTest.length) {
      setIsValid(true);

      if (onValidate) {
        onValidate(true);
      }
      return;
    }

    const noEmpty: boolean = valuesToTest.some((valueToTest) => (
      !!valueToTest.key && !!valueToTest.value
    ));

    let hasNoEmptyBeforeLast: boolean = true;
    if (valuesToTest.length > 1) {
      const withoutLast = [...valuesToTest];
      withoutLast.pop();
      hasNoEmptyBeforeLast = !withoutLast.some((valueToTest) => !valueToTest);
    }

    const valid = noEmpty && hasNoEmptyBeforeLast;
    setIsValid(valid);

    if (onValidate) {
      onValidate(valid);
    }
  };

  const onChangeKeyHandler = (
    evt: React.FormEvent<HTMLInputElement>,
    aIndex: number,
  ) => {
    const newValue = evt.currentTarget.value.trim();
    const tempAnswers = [...stateBadAnswers];

    tempAnswers[aIndex].key = newValue;
    setStateBadAnswers(tempAnswers);
  };

  const onChangeValueHandler = (
    evt: React.FormEvent<HTMLInputElement>,
    aIndex: number,
  ) => {
    const newValue = evt.currentTarget.value.trim();
    const tempBadAnswers = [...stateBadAnswers];
    tempBadAnswers[aIndex].value = newValue;
    setStateBadAnswers(tempBadAnswers);
  };

  const addHandler = () => {
    const newAnswers = [...stateBadAnswers];
    newAnswers.push({ key: '', value: '' });
    setStateBadAnswers(newAnswers);
  };

  const deleteHandler = (answerIndex: number) => {
    const newAnswers = stateBadAnswers.filter((answer, aIndex) => aIndex !== answerIndex);
    setStateBadAnswers(newAnswers);
  };

  const makeBadAnswers = (myAnswers: TBadAnswer[]) => {
    const answersToMake = myAnswers;

    let answerKey = -1;
    const answFields = answersToMake.map((badAnswer, aIndex) => {
      answerKey += answerKey;

      const validFieldKey: boolean = !!badAnswer.key.trim();
      const validFieldValue: boolean = !!badAnswer.value.trim();

      const deleteButton = (
        <Tooltip
          content={intl.formatMessage({ id: 'badAnswers.delete' })}
          wrapperTagName="div"
        >
          <Button
            icon="delete"
            intent={Intent.WARNING}
            minimal
            onClick={() => { deleteHandler(aIndex); }}
          />
        </Tooltip>
      );

      return (
        <div key={`bad-answer-input-${index}-${answerKey}`} className="bad-answer-item">
          <FormGroup
            helperText={validFieldKey ? '' : intl.formatMessage({ id: 'badAnswers.errorFieldKey' })}
            label={intl.formatMessage({ id: 'badAnswers.answer' }, { number: aIndex + 1 })}
            labelFor={`bad-answer-key-input-${index}-${answerKey}`}
            labelInfo={aIndex ? '' : `(${intl.formatMessage({ id: 'required' }, { gender: 'female' })})`}
            intent={validFieldKey ? Intent.SUCCESS : Intent.DANGER}
          >
            <InputGroup
              id={`bad-answer-key-input-${index}-${answerKey}`}
              placeholder={intl.formatMessage({ id: 'badAnswers.answerPlaceholder' })}
              value={badAnswer.key}
              intent={validFieldKey ? Intent.SUCCESS : Intent.DANGER}
              onChange={(evt: React.FormEvent<HTMLInputElement>) => {
                onChangeKeyHandler(evt, aIndex);
              }}
              leftIcon="thumbs-down"
              inputRef={badAnswer.key ? null : lastInput}
            />
          </FormGroup>
          <FormGroup
            helperText={validFieldValue ? '' : intl.formatMessage({ id: 'badAnswers.errorFieldValue' })}
            label={intl.formatMessage({ id: 'badAnswers.helper' }, { number: aIndex + 1 })}
            labelFor={`bad-answer-value-input-${index}-${answerKey}`}
            labelInfo={aIndex ? '' : `(${intl.formatMessage({ id: 'required' }, { gender: 'female' })})`}
            intent={validFieldValue ? Intent.SUCCESS : Intent.DANGER}
          >
            <InputGroup
              id={`bad-answer-value-input-${index}-${answerKey}`}
              placeholder={intl.formatMessage({ id: 'badAnswers.helperPlaceholder' })}
              value={badAnswer.value}
              intent={validFieldValue ? Intent.SUCCESS : Intent.DANGER}
              onChange={(evt: React.FormEvent<HTMLInputElement>) => {
                onChangeValueHandler(evt, aIndex);
              }}
              leftIcon="label"
            />
          </FormGroup>
          {deleteButton}
        </div>
      );
    });

    setAnswerFields(answFields);
  };

  useEffect(() => {
    setStateBadAnswers(badAnswers);
  }, [badAnswers]);

  useEffect(() => {
    makeBadAnswers(stateBadAnswers);

    validate(stateBadAnswers);

    if (onChange) {
      onChange(stateBadAnswers);
    }
  }, [stateBadAnswers]);

  useEffect(() => {
    if (lastInput.current) {
      lastInput.current.focus();
    }
  }, [answerFields]);

  const help = (
    <FormattedMessage
      id="help.badAnswers"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    <div className="field-group">
      <FormGroup
        helperText={isValid ? '' : intl.formatMessage({ id: 'badAnswers.error' })}
        label={intl.formatMessage({ id: 'badAnswers.label' })}
        intent={isValid ? Intent.SUCCESS : Intent.DANGER}
      >
        <Card className={isValid ? '' : 'invalid'}>
          {answerFields}
          <Button
            icon="add"
            intent={Intent.SUCCESS}
            title={intl.formatMessage({ id: 'correctAnswers.add' })}
            onClick={addHandler}
          />
        </Card>
      </FormGroup>
      <Help>{help}</Help>
    </div>
  );
};

BadAnswers.defaultProps = {
  onChange: null,
  onValidate: null,
};
export default BadAnswers;
