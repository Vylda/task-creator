import React, {
  FormEvent, useEffect, useRef, useState,
} from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import ISO6391 from 'iso-639-1';
import {
  FormGroup, InputGroup, Intent, Button,
} from '@blueprintjs/core';
import { getLanguages } from '../../selectors';
import {
  addNewLanguageAction, setCurrentLanguageAction,
  editLanguageAction, changeTasksLanguageAction,
} from '../../actions';

export enum Modes {
  'ADD',
  'EDIT',
}

interface IOnCloseProps {
  show: boolean;
  language: string;
  languages: string[];
  mode: Modes;
  onClose?: Function;
  addNewLanguage: Function;
  setCurrentLanguage: Function;
  editLanguage: Function;
  changeTasksLanguage: Function;
}

const LangForm = ({
  show, language, mode, languages, changeTasksLanguage,
  onClose, addNewLanguage, setCurrentLanguage, editLanguage,
}: IOnCloseProps) => {
  const intl = useIntl();
  const [currentValue, setCurrentValue] = useState(language);
  const [oldValue, setOldValue] = useState(language);
  const [isValid, setIsValid] = useState(false);

  const inputRef = useRef(null);

  const validate = (valueToTest: string): void => {
    const validLang = ISO6391.validate(valueToTest);
    const contains = mode === Modes.EDIT
      ? languages.filter((lng) => lng !== language).includes(valueToTest)
      : languages.includes(valueToTest);
    const valid = validLang && !contains;
    setIsValid(valid);
  };

  useEffect(() => {
    if (show && inputRef) {
      inputRef.current.focus();
      setCurrentValue(language);
      setOldValue(language);
      validate(language);
    }
  }, [show]);

  const close = (emmitValue: boolean = true) => {
    setCurrentValue('');
    if (onClose) {
      onClose(emmitValue ? currentValue : '');
    }
  };

  const valueChangeHandler = (evt: FormEvent): void => {
    const { currentTarget } = evt;
    const { value } = currentTarget as HTMLInputElement;
    const trimmedValue = value.toLowerCase().trim();
    validate(trimmedValue);
    setCurrentValue(trimmedValue);
  };

  const addClickHandler = () => {
    if (isValid) {
      addNewLanguage(currentValue);
      setCurrentLanguage(currentValue);
      close();
    }
  };

  const editClickHandler = () => {
    if (isValid) {
      editLanguage(currentValue, oldValue);
      changeTasksLanguage(currentValue, oldValue);
      setCurrentLanguage(currentValue);
      close(false);
    }
  };

  return (
    show
    && (
      <>
        <FormGroup
          helperText={isValid ? '' : intl.formatMessage({ id: 'language.error' })}
          label={intl.formatMessage({ id: 'language.newLabel' })}
          labelFor="language-id-input"
          labelInfo={`(${intl.formatMessage({ id: 'required' }, { gender: 'male' })})`}
          intent={isValid ? Intent.SUCCESS : Intent.DANGER}
        >
          <InputGroup
            id="language-id-input"
            placeholder={intl.formatMessage({ id: 'language.newPlaceholder' })}
            value={currentValue}
            intent={isValid ? Intent.SUCCESS : Intent.DANGER}
            onChange={valueChangeHandler}
            leftIcon="translate"
            inputRef={inputRef}
          />
        </FormGroup>
        { mode === Modes.ADD
          && (
            <Button
              icon="small-plus"
              intent={Intent.PRIMARY}
              disabled={!isValid}
              onClick={addClickHandler}
            >
              <FormattedMessage id="language.add" />
            </Button>
          )}
        { mode === Modes.EDIT
          && (
            <Button
              icon="edit"
              intent={Intent.PRIMARY}
              disabled={!isValid || currentValue === oldValue}
              onClick={editClickHandler}
            >
              <FormattedMessage id="language.edit" />
            </Button>
          )}
        <Button
          icon="cross"
          intent={Intent.NONE}
          onClick={() => close(false)}
          title={intl.formatMessage({ id: 'close' })}
        />
      </>
    ));
};

LangForm.defaultProps = {
  onClose: null,
};

const mapStateToProps = (state: any) => ({
  languages: getLanguages(state),
});

const mapDispatchToProps = {
  addNewLanguage: addNewLanguageAction,
  setCurrentLanguage: setCurrentLanguageAction,
  editLanguage: editLanguageAction,
  changeTasksLanguage: changeTasksLanguageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(LangForm);
