import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import Help from '../Help';

interface IQuestion {
  disabled: boolean;
  value: string;
  index: number;
  onChange?: Function;
  onValidate?: Function;
}

const Question = ({
  value, disabled, index, onChange, onValidate,
}: IQuestion) => {
  const intl = useIntl();
  const [isValid, setIsValid] = useState(false);

  const validate = (valueToTest: string): void => {
    const valid = valueToTest !== '';
    setIsValid(valid);
    if (onValidate) {
      onValidate(valid);
    }
  };

  useEffect(() => {
    validate(value);
  }, [value]);

  const onChangeHandler = (evt: React.FormEvent<HTMLInputElement>): void => {
    const newValue = evt.currentTarget.value.trim();

    validate(newValue);

    if (onChange) {
      onChange(newValue);
    }
  };

  const help = (
    <FormattedMessage
      id="help.question"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    <div className="field-group">
      <FormGroup
        helperText={isValid ? '' : intl.formatMessage({ id: 'question.error' })}
        label={intl.formatMessage({ id: 'question.label' })}
        labelFor={`question-input-${index}`}
        labelInfo={`(${intl.formatMessage({ id: 'required' }, { gender: 'female' })})`}
        disabled={disabled}
        intent={isValid ? Intent.SUCCESS : Intent.DANGER}
      >
        <InputGroup
          id={`question-input-${index}`}
          placeholder={intl.formatMessage({ id: 'question.placeholder' })}
          value={value}
          readOnly={disabled}
          intent={isValid ? Intent.SUCCESS : Intent.DANGER}
          onChange={onChangeHandler}
          leftIcon="help"
        />
      </FormGroup>
      <Help>{help}</Help>
    </div>
  );
};

Question.defaultProps = {
  onChange: null,
  onValidate: null,
};
export default Question;
