import React, {
  useEffect, useRef, useState,
} from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import ISO6391 from 'iso-639-1';
import {
  FormGroup, FileInput, Intent, Button, Icon,
} from '@blueprintjs/core';
import {
  ITask, ITasksObject, TBadAnswer, defaultTask,
} from '../../reducers/tasks';
import {
  TLanguage,
} from '../../reducers/language';
import {
  setAllTasksAction, setAllLanguagesAction,
} from '../../actions';

interface IUploadProps {
  setAllTasks: Function;
  setAllLanguages: Function;
  onClose: Function;
}

const Upload = ({ setAllTasks, setAllLanguages, onClose }: IUploadProps) => {
  const [fileUploaded, setFileUploaded] = useState(false);
  const [uploadError, setUploadError] = useState('');
  const [tasksObject, setTasksObject] = useState(null);
  const [resetDone, setResetDone] = useState(false);

  const { formatMessage } = useIntl();
  const container = useRef(null);

  const validateLanguage = (data: ITasksObject): boolean => {
    const keys: string[] = Object.keys(data);
    if (!keys.length) {
      return false;
    }
    const result = !keys.some((key: string) => !ISO6391.validate(key));

    return result;
  };

  const languagesIsNotArray = (data: ITasksObject): string[] => {
    const keys: string[] = Object.keys(data);
    const badLanguages = keys.reduce((acc: string[], key: string) => {
      if (!Array.isArray(data[key])) {
        acc.push(key);
      }
      return acc;
    }, []);
    return badLanguages;
  };

  const compareKeys = (a: any, b: any = defaultTask): Boolean => {
    const aKeys = Object.keys(a).sort();
    const bKeys = Object.keys(b).sort();
    return JSON.stringify(aKeys) === JSON.stringify(bKeys);
  };

  const validateLevel = (level: number): boolean => (
    Number.isInteger(level)
    && level >= 0
  );

  const validateQuestion = (question: string): boolean => typeof question === 'string';

  const validateStringArray = (stringArray: string[]): boolean => {
    if (!Array.isArray(stringArray)) {
      return false;
    }

    const notAllStrings = stringArray.some((answer: string) => {
      const isString = typeof answer === 'string';
      return !isString;
    });

    return !notAllStrings;
  };

  const validateBadAnswers = (badAnswers: TBadAnswer[]): boolean => {
    if (!Array.isArray(badAnswers)) {
      return false;
    }

    const hasBadData = badAnswers.some((badAnswer: TBadAnswer) => {
      const defaultBadAnswer: TBadAnswer = {
        key: '',
        value: '',
      };
      if (!compareKeys(badAnswer, defaultBadAnswer)) {
        return true;
      }

      return (typeof badAnswer.key !== 'string' || typeof badAnswer.value !== 'string');
    });

    return !hasBadData;
  };

  const validateBoolean = (value: boolean): boolean => typeof value === 'boolean';

  const validateTasks = (tasks: ITask[]) => {
    const tasksAreNotValid = tasks.some((task: ITask) => {
      const validKeys = compareKeys(task);
      const validLevel = validateLevel(task.level);
      const validQuestion = validateQuestion(task.question);
      const validCorrectAnswers = validateStringArray(task.correctAnswers);
      const validHelpers = validateStringArray(task.helpers);
      const validBadAnswers = validateBadAnswers(task.badAnswers);
      const validIndex = validateLevel(task.index);
      const validValid = validateBoolean(task.valid);

      return !(validKeys && validLevel && validQuestion && validCorrectAnswers && validHelpers
        && validBadAnswers && validIndex && validValid);
    });

    return !tasksAreNotValid;
  };

  const tasksNotCorrect = (data: ITasksObject): string[] => {
    const keys: string[] = Object.keys(data);
    const badLanguages = keys.reduce((acc: string[], key: string) => {
      if (!validateTasks(data[key])) {
        acc.push(key);
      }
      return acc;
    }, []);

    return badLanguages;
  };

  const readFile = (file: File) => {
    const reader = new FileReader();

    reader.addEventListener('load', () => {
      const result: string = reader.result as string;
      try {
        const data: ITasksObject = JSON.parse(result);

        if (!validateLanguage(data)) {
          setUploadError(formatMessage({ id: 'upload.errorBadKeys' }));
          return;
        }

        const langNoArray = languagesIsNotArray(data);
        if (langNoArray.length) {
          setUploadError(formatMessage(
            { id: 'upload.errorNotArray' },
            {
              languages: langNoArray.join(', '),
              langNum: langNoArray.length,
            },
          ));
          return;
        }

        const tasksNotCorect: string[] = tasksNotCorrect(data);
        if (tasksNotCorect.length) {
          setUploadError(formatMessage(
            { id: 'upload.errorTasksNotCorrect' },
            {
              languages: tasksNotCorect.join(', '),
              langNum: tasksNotCorect.length,
            },
          ));
          return;
        }

        setTasksObject(data);
        setFileUploaded(true);
      } catch (error) {
        console.error(reader.error);
        setUploadError(formatMessage({ id: 'upload.errorNotValidJSON' }));
      }
    });

    reader.addEventListener('error', () => {
      console.error(reader.error);
      setUploadError(formatMessage({ id: 'upload.errorReader' }));
    });

    reader.readAsText(file);
  };

  const proccessUpload = (dropFiles: FileList) => {
    if (!dropFiles.length) {
      return;
    }

    if (dropFiles.length > 1) {
      setUploadError(formatMessage({ id: 'upload.errorTooMuchFiles' }));
      return;
    }

    const file: File = dropFiles[0];
    if (file.type !== 'application/json') {
      setUploadError(formatMessage({ id: 'upload.errorNotJson' }));
      return;
    }

    readFile(file);
  };

  const changeFileInputHandler = (event: React.FormEvent<HTMLInputElement>) => {
    setFileUploaded(false);
    setUploadError('');
    setTasksObject(null);
    proccessUpload(event.currentTarget.files);
  };

  const dropHandler = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();
    setFileUploaded(false);
    setUploadError('');
    setTasksObject(null);

    if (container.current) {
      container.current.classList.remove('is-dragover');
    }

    proccessUpload(event.dataTransfer.files);
  };

  const dragHandler = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const dragEnterHandler = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();
    setFileUploaded(false);
    setUploadError('');
    setTasksObject(null);
    if (container.current) {
      container.current.classList.add('is-dragover');
    }
  };

  const dragLeaveHandler = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    event.stopPropagation();

    if (container.current) {
      container.current.classList.remove('is-dragover');
    }
  };

  const onClickUploadJsonHandler = () => {
    setAllTasks({});
    setAllLanguages({ current: '', languages: [] });
    setResetDone(true);
  };

  useEffect(() => {
    if (resetDone) {
      const languages = Object.keys(tasksObject);
      const languageSettings: TLanguage = {
        current: languages[0],
        languages,
      };
      setResetDone(false);
      setAllLanguages(languageSettings);
      setAllTasks(tasksObject);
      onClose();
    }
  });

  const label = (
    <FormattedMessage
      id="upload.label"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    <div
      className="box-input"
      onDrag={dragHandler}
      onDragStart={dragHandler}
      onDragEnd={dragLeaveHandler}
      onDragOver={dragEnterHandler}
      onDragEnter={dragEnterHandler}
      onDragLeave={dragLeaveHandler}
      onDrop={dropHandler}
      ref={container}
    >
      <Icon
        icon="inbox"
        iconSize={60}
      />
      <FormGroup
        label={label}
        labelFor="settings-file-input"
      >
        <FileInput
          text={formatMessage({ id: 'upload.placeholder' })}
          buttonText={formatMessage({ id: 'upload.buttonText' })}
          inputProps={{
            accept: '.json',
            id: 'settings-file-input',
          }}
          onInputChange={changeFileInputHandler}
        />
      </FormGroup>
      {uploadError && (
        <div className="error">
          {uploadError}
        </div>
      )}
      <Button
        className="box-button"
        intent={Intent.SUCCESS}
        disabled={!fileUploaded}
        onClick={onClickUploadJsonHandler}
      >
        <FormattedMessage id="settings.title" />
      </Button>
    </div>
  );
};

const mapDispatchToProps = {
  setAllTasks: setAllTasksAction,
  setAllLanguages: setAllLanguagesAction,
};

export default connect(null, mapDispatchToProps)(Upload);
