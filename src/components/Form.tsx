import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import ISO6391 from 'iso-639-1';
import {
  Button, Intent,
} from '@blueprintjs/core';
import { getTasks, getDialogDone, getCurrentLanguage } from '../selectors';
import { ITask, ITasksObject } from '../reducers/tasks';
import {
  addTaskAction, removeTaskAction, setOpenedTaskIndexAction, setDialogAction,
  addTasksAction,
} from '../actions';
import { PRIMARY_LANGUAGE } from '../constants';
import Task from './Task';

type TForm = {
  tasks: ITasksObject;
  dialogDone: boolean;
  language: string;
  addTask: Function;
  addTasks: Function;
  removeTask: Function,
  setOpenedTaskIndex: Function;
  setDialog: Function;
};

const Form = ({
  tasks, dialogDone, language,
  addTask, removeTask, setOpenedTaskIndex, setDialog, addTasks,
}: TForm) => {
  const [tasksArray, setTaskArray] = useState([]);
  const [deleteIndex, setDeleteIndex] = useState(-1);

  const { formatMessage } = useIntl();

  const deleteTask = (index: number) => {
    if (index > 0) {
      removeTask(index);
      setOpenedTaskIndex(-1);
      setDeleteIndex(-1);
    }
  };

  useEffect(() => {
    if (dialogDone && deleteIndex > -1) {
      deleteTask(deleteIndex);
      setDialog(false);
    }
  }, [dialogDone]);

  const removeHandler = (index: number, arrayIndex: number) => {
    setDeleteIndex(index);
    setDialog(
      true,
      formatMessage({ id: 'deleteTaskLabel' }, { index: arrayIndex + 1 }),
      <p><FormattedMessage id="deleteTaskBody" /></p>,
    );
  };

  const addHandler = () => {
    const maxIndex: number = tasksArray.reduce((i, task) => {
      const index = i > task.props.index ? i : task.props.index;
      return index;
    }, 0) + 1;
    addTask(maxIndex, language);
    setOpenedTaskIndex(maxIndex);
  };

  useEffect(() => {
    if (language) {
      const hasItemInLanguage: boolean = !!tasks[language];
      if (!hasItemInLanguage) {
        const tasksLangObj = tasks[PRIMARY_LANGUAGE] || tasks[Object.keys(tasks)[0]];
        const primaryChangedTasks = [...tasksLangObj]
          .map((task) => ({ ...task }));
        addTasks(primaryChangedTasks, language);
      }
    }
  }, [language]);

  useEffect(() => {
    const tasksMap = tasks[language]?.map(
      (task: ITask, index: number) => (
        <Task
          task={task}
          language={language}
          arrayIndex={index}
          index={task.index}
          key={`task-${language}-${task.index}`}
          onRemove={removeHandler}
        />
      ),
    );
    setTaskArray(tasksMap);
  }, [tasks, language]);

  return (
    <>
      <h2>
        <FormattedMessage
          id="tasksHeader"
          values={{
            language,
            native: ISO6391.getNativeName(language),
            name: ISO6391.getName(language),
          }}
        />
      </h2>
      {tasksArray}
      <div className="form-buttons">
        <Button
          intent={Intent.SUCCESS}
          onClick={() => addHandler()}
          title={formatMessage({ id: 'task.add' })}
          icon="plus"
        >
          <FormattedMessage id="task.add" />
        </Button>
      </div>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  tasks: getTasks(state),
  dialogDone: getDialogDone(state),
  language: getCurrentLanguage(state),
});

const mapDispatchToProps = {
  addTask: addTaskAction,
  removeTask: removeTaskAction,
  setOpenedTaskIndex: setOpenedTaskIndexAction,
  setDialog: setDialogAction,
  addTasks: addTasksAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
