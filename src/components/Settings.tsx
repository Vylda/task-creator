import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import {
  Button, Intent, Overlay, Card, Elevation, Classes,
} from '@blueprintjs/core';
import fileDownload from 'js-file-download';
import { getTasks } from '../selectors';
import { ITasksObject } from '../reducers/tasks';
import Upload from './inputs/Upload';
import Help from './Help';

interface ISettingsProps {
  tasks: ITasksObject;
}

const Settings = ({
  tasks,
}: ISettingsProps) => {
  const [isUploadable, setIsUploadable] = useState(false);
  const [showOverlay, setShowOverlay] = useState(false);

  const isAdvancedUpload = () => {
    const div = document.createElement('div');
    return (
      ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)
    )
      && 'FormData' in window
      && 'FileReader' in window;
  };

  useEffect(() => {
    setIsUploadable(isAdvancedUpload());
  }, []);

  const downloadHandler = (taskObject: ITasksObject) => {
    const code = JSON.stringify(taskObject, null, 2);
    fileDownload(code, 'answer-responder-settings.json');
  };

  const uploadHandler = () => {
    setShowOverlay(true);
  };

  const onClose = () => {
    setShowOverlay(false);
  };

  const help = (
    <FormattedMessage
      id="help.settings"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    isUploadable && (
      <div id="settings">
        <h2>
          <span>
            <FormattedMessage id="settings.header" />
          </span>
          <Help>{help}</Help>
        </h2>
        <div className="settings-button-line">
          <Button
            icon="download"
            onClick={() => downloadHandler(tasks)}
            intent={Intent.PRIMARY}
          >
            <FormattedMessage id="settings.download" />
          </Button>
          <Button
            icon="upload"
            onClick={() => uploadHandler()}
            intent={Intent.PRIMARY}
          >
            <FormattedMessage id="settings.upload" />
          </Button>
        </div>
        <Overlay
          isOpen={showOverlay}
          className={Classes.OVERLAY_SCROLL_CONTAINER}
          portalClassName="load-file"
          autoFocus
          enforceFocus
          canOutsideClickClose={false}
          canEscapeKeyClose
        >
          <Card elevation={Elevation.THREE} className="settings-content">
            <h2 className="bp3-heading">
              <FormattedMessage id="settings.title" />
            </h2>
            <Upload onClose={onClose} />
            <div className="button-line">
              <Button
                onClick={() => { setShowOverlay(false); }}
              >
                <FormattedMessage id="close" />
              </Button>
            </div>
          </Card>
        </Overlay>
      </div>
    ));
};

const mapStateToProps = (state: any) => ({
  tasks: getTasks(state),
});

export default connect(mapStateToProps)(Settings);
