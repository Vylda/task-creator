import React from 'react';
import { useIntl } from 'react-intl';
import { Icon, Intent, Popover } from '@blueprintjs/core';

type IHelpProps = {
  children?: string | JSX.Element;
};

const Help = ({ children }: IHelpProps) => {
  const { formatMessage } = useIntl();

  return (
    <Popover
      content={children}
      isOpen={undefined}
      hasBackdrop
      canEscapeKeyClose
      usePortal
    >
      <Icon
        icon="help"
        htmlTitle={formatMessage({ id: 'help.help' })}
        intent={Intent.PRIMARY}
      />
    </Popover>
  );
};

Help.defaultProps = {
  children: '',
};
export default Help;
