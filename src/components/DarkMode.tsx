import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Switch, Alignment } from '@blueprintjs/core';

export enum MODES {
  'NONE',
  'DARK',
  'LIGHT',
}

type IDarkModeProps = {
  useDarkMode?: boolean;
  defaultMode?: MODES;
  onChange?: Function;
  switchTitleKey?: string;
};

const DarkMode = ({
  useDarkMode, defaultMode, onChange, switchTitleKey,
}: IDarkModeProps) => {
  const [darkModeState, setDarkModeState] = useState(defaultMode);
  const { formatMessage } = useIntl();

  const setDarkMode = (mode: MODES) => {
    setDarkModeState(mode);
    const body = document.querySelector('body');
    if (body) {
      body.classList[mode === MODES.DARK ? 'add' : 'remove']('bp3-dark');
    }
  };

  const setPrefferedMode = () => {
    const prefersDarkScheme = window.matchMedia('(prefers-color-scheme: dark)');
    const mode = prefersDarkScheme.matches ? MODES.DARK : MODES.LIGHT;
    setDarkMode(mode);
  };

  useEffect(() => {
    if (useDarkMode && defaultMode === MODES.NONE) {
      setPrefferedMode();
    }
  }, []);

  useEffect(() => {
    if (defaultMode !== MODES.NONE) {
      setDarkMode(defaultMode);
    } else {
      setPrefferedMode();
    }
  }, [defaultMode]);

  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { checked } = event.currentTarget;
    const mode = checked ? MODES.DARK : MODES.LIGHT;

    setDarkMode(mode);

    if (onChange) {
      onChange(mode);
    }
  };

  return useDarkMode && (
    <div id="dark-mode-switch">
      <Switch
        checked={darkModeState === MODES.DARK}
        label={switchTitleKey && formatMessage({ id: switchTitleKey })}
        onChange={handleChange}
        alignIndicator={Alignment.LEFT}
      />
    </div>
  );
};

DarkMode.defaultProps = {
  useDarkMode: true,
  defaultMode: MODES.NONE,
  onChange: null,
  switchTitleKey: null,
};
export default DarkMode;
