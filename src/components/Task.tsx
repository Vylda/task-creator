import React, { useEffect, useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { connect } from 'react-redux';
import {
  Button, Collapse, Icon, Intent,
} from '@blueprintjs/core';
import { getOpenedTaskIndex } from '../selectors';
import { ITask, TBadAnswer } from '../reducers/tasks';
import { setOpenedTaskIndexAction, changeTaskAction } from '../actions';
import Question from './inputs/Question';
import CorrectAnswers from './inputs/CorrectAnswers';
import Level from './inputs/Level';
import BadAnswers from './inputs/BadAnswers';
import AnswerHelpers from './inputs/AnswerHelpers';

type IValidState = {
  [key: string]: boolean;
};

type ITaskElement = {
  task: ITask;
  index: number;
  arrayIndex: number;
  language: string;
  openedTaskIndex: number;
  onRemove: Function;
  setOpenedTaskIndex: Function;
  changeTask: Function;
};

const Task = ({
  task,
  index,
  arrayIndex,
  language,
  openedTaskIndex,
  setOpenedTaskIndex,
  changeTask,
  onRemove,
}: ITaskElement) => {
  const defaultValid: IValidState = {
    level: false,
    question: false,
    correctAnswers: false,
    badAnswers: false,
  };

  const [isOpen, setIsOpen] = useState(false);
  const [myTask, setMyTask] = useState(task);
  const [validTask, setValidTask] = useState(task.valid);

  const validElementsRef = useRef(defaultValid);

  const { formatMessage } = useIntl();
  const timeOut = useRef(null);

  useEffect(() => {
    const tempTask: ITask = { ...myTask };
    tempTask.valid = validTask;
    if (timeOut.current) {
      clearTimeout(timeOut.current);
      timeOut.current = null;
    }
    timeOut.current = setTimeout(() => {
      changeTask(tempTask, arrayIndex, language);
      timeOut.current = null;
    }, 300);
  }, [myTask]);

  useEffect(() => {
    const open = openedTaskIndex === index;
    setIsOpen(open);
  }, [openedTaskIndex]);

  const onSwitchOpenHandler = () => {
    setOpenedTaskIndex(index);
  };

  const changeCorrectAnswersHandler = (correctAnswers: string[]): void => {
    const tempTask: ITask = { ...myTask, correctAnswers };
    setMyTask(tempTask);
  };

  const changeQuestionHandler = (question: string): void => {
    const tempTask: ITask = { ...myTask, question };
    setMyTask(tempTask);
  };

  const changeLevelHandler = (level: number): void => {
    const tempTask: ITask = { ...myTask, level };
    setMyTask(tempTask);
  };

  const changeBadAnswersHandler = (badAnswers: TBadAnswer[]): void => {
    const tempTask: ITask = { ...myTask, badAnswers };
    setMyTask(tempTask);
  };

  const changeAnswerHelpersHandler = (helpers: string[]): void => {
    const tempTask: ITask = { ...myTask, helpers };
    setMyTask(tempTask);
  };

  const validateHandler = (type: string, valid: boolean): void => {
    validElementsRef.current[type] = valid;
    const taskIsValid: boolean = !Object.keys(validElementsRef.current).some((key: string) => (
      !validElementsRef.current[key]
    ));

    setValidTask(taskIsValid);
  };

  const removeHandler = () => {
    onRemove(index, arrayIndex);
  };

  return (
    <div className="task-item">
      <div className="task-item-header">
        <div className="task-item-buttons">
          <Button
            icon={isOpen ? 'menu-closed' : 'menu-open'}
            onClick={onSwitchOpenHandler}
          >
            <span className="button-question">
              <FormattedMessage id="task.number" values={{ number: arrayIndex + 1, question: myTask.question, language }} />
            </span>
          </Button>
          {isOpen && index > 0 && (
            <Button
              icon="remove"
              onClick={removeHandler}
              intent={Intent.WARNING}
            >
              <FormattedMessage id="task.delete" />
            </Button>
          )}
        </div>
        {!validTask && (
          <Icon
            icon="error"
            intent={Intent.DANGER}
            iconSize={Icon.SIZE_LARGE}
            tagName="div"
            title={formatMessage({ id: 'task.bad' })}
            htmlTitle={formatMessage({ id: 'task.bad' })}
            className="bad-task-icon"
          />
        )}
      </div>
      <Collapse isOpen={isOpen}>
        <Question
          disabled={index === 0}
          value={myTask.question}
          index={index}
          onChange={(val: string) => {
            changeQuestionHandler(val);
          }}
          onValidate={(valid: boolean) => { validateHandler('question', valid); }}
        />
        <CorrectAnswers
          index={index}
          answers={myTask.correctAnswers}
          onChange={(ans: string[]) => {
            changeCorrectAnswersHandler(ans);
          }}
          onValidate={(valid: boolean) => { validateHandler('correctAnswers', valid); }}
        />
        <Level
          value={task.level}
          index={index}
          onChange={(val: number) => {
            changeLevelHandler(val);
          }}
          onValidate={(valid: boolean) => { validateHandler('level', valid); }}
          disabled={arrayIndex === 0}
        />
        <BadAnswers
          badAnswers={myTask.badAnswers}
          index={index}
          onChange={(val: TBadAnswer[]) => {
            changeBadAnswersHandler(val);
          }}
          onValidate={(valid: boolean) => { validateHandler('badAnswers', valid); }}
        />
        <AnswerHelpers
          helpers={myTask.helpers}
          answers={myTask.correctAnswers}
          index={index}
          onChange={(val: string[]) => {
            changeAnswerHelpersHandler(val);
          }}
        />
      </Collapse>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  openedTaskIndex: getOpenedTaskIndex(state),
});

const mapDispatchToProps = {
  setOpenedTaskIndex: setOpenedTaskIndexAction,
  changeTask: changeTaskAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Task);
