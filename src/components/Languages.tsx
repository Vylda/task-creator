import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage, useIntl } from 'react-intl';
import {
  Card, Elevation, MenuItem, Button, Intent,
} from '@blueprintjs/core';
import { Select, ItemRenderer } from '@blueprintjs/select';
import { highlightText, filterItems } from '../utils';
import { getLanguages, getCurrentLanguage, getDialogDone } from '../selectors';
import {
  setCurrentLanguageAction, removeLanguageAction, setDialogAction,
  removeLanguageTasksAction,
} from '../actions';
import LangForm, { Modes } from './inputs/LangForm';
import Help from './Help';

type TLangs = {
  languages: string[];
  current: string;
  dialogDone: boolean;
  setCurrentLanguage: Function;
  removeLanguageTasks: Function;
  removeLanguage: Function;
  setDialog: Function;
};

const Languages = ({
  languages, current, dialogDone, removeLanguageTasks,
  setCurrentLanguage, removeLanguage, setDialog,
}: TLangs) => {
  const [options, setOptions] = useState(languages);
  const [showAddLangForm, setShowAddLangForm] = useState(false);
  const [activeElement, setActiveElement] = useState(null);
  const [currentLang, setCurrentLang] = useState('');
  const [currentMode, setCurrentMode] = useState(null);
  const [langToDelete, setLangToDelete] = useState('');
  const { formatMessage } = useIntl();

  useEffect(() => {
    const items = languages
      .filter((lang) => lang);
    setOptions(items);
    if (!currentLang) {
      setCurrentLang(current);
    }
  }, [languages]);

  useEffect(() => {
    if (dialogDone && langToDelete) {
      removeLanguage(langToDelete);
      removeLanguageTasks(langToDelete);
      setLangToDelete('');
      setDialog(false);
    }
  }, [dialogDone, langToDelete]);

  const renderItem: ItemRenderer<string> = (item: string, { handleClick, modifiers, query }) => {
    const element = (
      <MenuItem
        active={modifiers.active}
        key={item}
        text={highlightText(item, query)}
        onClick={handleClick}
      />
    );
    return element;
  };

  const addLanguageHandler = () => {
    setCurrentMode(Modes.ADD);
    setShowAddLangForm(true);
  };

  const handleActiveItemChange = (activeItem: MenuItem) => {
    if (!activeItem) {
      return;
    }

    if (activeItem && currentLang) {
      if (activeItem.toString() === currentLang) {
        setTimeout(() => {
          setActiveElement(activeItem);
          setCurrentLanguage(activeItem.toString());
        }, 0);
        setCurrentLang('');
      }
      return;
    }

    if (currentMode === null) {
      setActiveElement(activeItem);
      setCurrentLanguage(activeItem.toString());
    } else {
      setCurrentMode(null);
    }
  };

  const editLanguageHandler = () => {
    setCurrentMode(Modes.EDIT);
    setShowAddLangForm(true);
  };

  const removeLanguageHandler = () => {
    const lang = activeElement.toString();
    setLangToDelete(lang);
    setDialog(
      true,
      formatMessage(
        { id: 'language.deleteHeader' },
        { language: lang },
      ),
      <p><FormattedMessage id="language.deleteBody" /></p>,
    );
  };

  const help = (
    <FormattedMessage
      id="help.language"
      values={{
        b: (chunks: string) => <b>{chunks}</b>,
      }}
    />
  );

  return (
    <Card
      elevation={Elevation.ONE}
      className="language-section"
    >
      <h2>
        <span>
          <FormattedMessage id="language.header" />
        </span>
        <Help>{help}</Help>
      </h2>
      <div className="buttons">
        <Select
          items={options}
          onItemSelect={handleActiveItemChange}
          onActiveItemChange={handleActiveItemChange}
          itemRenderer={renderItem}
          activeItem={activeElement}
          noResults={<MenuItem disabled text={formatMessage({ id: 'language.noResult' })} />}
          itemPredicate={filterItems}
          query={currentLang}
        >
          <Button
            text={activeElement || formatMessage({ id: 'language.noSelect' })}
            rightIcon="double-caret-vertical"
            className="language-select-button"
            disabled={showAddLangForm}
          />
        </Select>
        <Button
          text={formatMessage({ id: 'language.add' })}
          icon="plus"
          intent={Intent.SUCCESS}
          onClick={addLanguageHandler}
          disabled={showAddLangForm}
        />
        <Button
          text={formatMessage({ id: 'language.edit' })}
          icon="edit"
          intent={Intent.PRIMARY}
          onClick={editLanguageHandler}
          disabled={showAddLangForm}
        />
        <Button
          text={formatMessage({ id: 'language.remove' })}
          icon="trash"
          intent={Intent.WARNING}
          onClick={removeLanguageHandler}
          disabled={showAddLangForm || options.length <= 1}
        />
      </div>
      <div id="language-editor">
        <LangForm
          show={showAddLangForm}
          language={currentMode === 1 ? activeElement.toString() : ''}
          mode={currentMode}
          onClose={(language: string) => {
            setCurrentLang(language);
            setShowAddLangForm(false);
            if (!language) {
              setCurrentMode(null);
            }
          }}
        />
      </div>
    </Card>
  );
};

const mapStateToProps = (state: any) => ({
  languages: getLanguages(state),
  current: getCurrentLanguage(state),
  dialogDone: getDialogDone(state),
});

const mapDispatchToProps = {
  setCurrentLanguage: setCurrentLanguageAction,
  removeLanguage: removeLanguageAction,
  removeLanguageTasks: removeLanguageTasksAction,
  setDialog: setDialogAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Languages);
