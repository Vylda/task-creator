import React, { ReactNode } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import {
  Dialog, Button, Classes, Intent, IconName,
} from '@blueprintjs/core';
import { setDialogAction, setDialogDoneAction } from '../actions';
import {
  getDialogIsOpen, getDialogIcon, getDialogTitle, getDialogBody,
} from '../selectors';

interface IMyDialog {
  title: string;
  body?: ReactNode;
  icon: IconName;
  show: boolean;
  setDialog: Function;
  setDialogDone: Function;
}

const MyDialog = ({
  title, body, icon, show,
  setDialog, setDialogDone,
}: IMyDialog) => {
  const closeDialogHandler = () => {
    setDialog(false);
  };

  const successHandler = () => {
    setDialogDone(true);
  };

  return (
    <Dialog
      className="task-item-dialog"
      title={title}
      icon={icon}
      isOpen={show}
      onClose={closeDialogHandler}
    >
      <div className={Classes.DIALOG_BODY}>
        {body && <div className="my-dialog-body">{body}</div>}
        <div className="button-line">
          <Button
            onClick={closeDialogHandler}
            icon="undo"
          >
            <FormattedMessage id="no" />
          </Button>
          <Button
            icon="tick"
            intent={Intent.WARNING}
            onClick={successHandler}
          >
            <FormattedMessage id="yes" />
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

const mapStateToProps = (state: any) => ({
  show: getDialogIsOpen(state),
  icon: getDialogIcon(state),
  title: getDialogTitle(state),
  body: getDialogBody(state),
});

const mapDispatchToProps = {
  setDialog: setDialogAction,
  setDialogDone: setDialogDoneAction,
};

MyDialog.defaultProps = {
  body: '',
};
export default connect(mapStateToProps, mapDispatchToProps)(MyDialog);
