import React, { useEffect, useState } from 'react';
import { Card, Elevation } from '@blueprintjs/core';
import { IntlProvider } from 'react-intl';
import { langCode, messages, getTranslations } from './langResolver';
import Header from './components/Header';
import Languages from './components/Languages';
import Form from './components/Form';
import Code from './components/Code';
import Settings from './components/Settings';
import MyDialog from './components/MyDialog';
import DarkMode, { MODES } from './components/DarkMode';
import Translations from './components/Translations';
import DeleteSettings from './components/DeleteSettings';

const App = () => {
  const [localeState, setLocaleState] = useState(langCode);
  const [messagesState, setMessagesState] = useState(messages);
  const [defaultMode, setdefaultMode] = useState(MODES.NONE);

  useEffect(() => {
    const modeNumber: number = parseInt(localStorage.getItem('tcDarkMode'), 10) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);
  }, []);

  const languageChangeHandler = (newLanguage: string) => {
    localStorage.setItem('defaultLanguage', newLanguage);
    const newTranslation = getTranslations();
    setMessagesState(newTranslation.messages);
    setLocaleState(newTranslation.langCode);
  };

  const modeChangeHandler = (mode: MODES) => {
    localStorage.setItem('tcDarkMode', mode.toString());
  };

  const deleteSettingsHandler = () => {
    localStorage.removeItem('tcDarkMode');
    localStorage.removeItem('defaultLanguage');
    const newTranslation = getTranslations();
    setLocaleState(newTranslation.langCode);
    setMessagesState(newTranslation.messages);
    setdefaultMode(MODES.NONE);
  };

  useEffect(() => {
    const modeNumber: number = parseInt(localStorage.getItem('tcDarkMode'), 10) || 0;
    const mode = modeNumber as MODES;
    setdefaultMode(mode);
  }, [localeState]);

  return (
    <IntlProvider key={localeState} locale={localeState} messages={messagesState}>
      <Card elevation={Elevation.TWO}>
        <Card elevation={Elevation.ZERO} className="app-settings">
          <DarkMode switchTitleKey="darkMode" defaultMode={defaultMode} onChange={modeChangeHandler} />
          <Translations
            defaultLanguage={localeState}
            onChange={languageChangeHandler}
            buttonTitleKey="setTranslation"
          />
          <DeleteSettings onClick={deleteSettingsHandler} />
        </Card>
        <Header />
        <Languages />
        <Form />
        <Code />
        <Settings />
        <MyDialog />
      </Card>
    </IntlProvider>
  );
};

export default App;
