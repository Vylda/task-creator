export const PRIMARY_LANGUAGE = 'cs';

export const ADD_TASK: string = 'ADD_TASK';
export const ADD_TASKS: string = 'ADD_TASKS';
export const CHANGE_TASK: string = 'CHANGE_TASK';
export const CHANGE_OPEN_TASK_INDEX: string = 'CHANGE_OPEN_TASK_INDEX';
export const REMOVE_TASK: string = 'REMOVE_TASK';
export const CHANGE_TASKS_LANGUAGE = 'CHANGE_TASKS_LANGUAGE';
export const CHANGE_LANGUAGE_NAME: string = 'CHANGE_LANGUAGE_NAME';
export const SET_LANGUAGE: string = 'SET_LANGUAGE';
export const REMOVE_LANGUAGE: string = 'REMOVE_LANGUAGE';
export const REMOVE_TASKS_LANGUAGE: string = 'REMOVE_TASKS_LANGUAGE';
export const ADD_NEW_LANGUAGE: string = 'ADD_NEW_LANGUAGE';
export const SET_DIALOG_OPEN: string = 'SET_DIALOG_OPEN';
export const SET_DIALOG_DONE: string = 'SET_DIALOG_DONE';
export const SET_TASKS: string = 'SET_TASKS';
export const SET_ALL_LANGUAGES: string = 'SET_ALL_LANGUAGES';
