import {
  ADD_TASK, CHANGE_TASK, REMOVE_TASK, REMOVE_TASKS_LANGUAGE,
  PRIMARY_LANGUAGE, ADD_TASKS, CHANGE_TASKS_LANGUAGE, SET_TASKS,
} from '../constants';

export type TBadAnswer = {
  key: string;
  value: string;
};

export interface ITask {
  question: string;
  level: number;
  correctAnswers: string[];
  badAnswers: TBadAnswer[];
  helpers: string[];
  valid: boolean;
  index: number;
}

export const defaultTask: ITask = {
  level: 0,
  question: '',
  correctAnswers: [],
  badAnswers: [],
  helpers: [],
  valid: false,
  index: 0,
};

export interface ITasksObject {
  [key: string]: ITask[];
}

const firstTask: ITask = {
  ...defaultTask,
  question: '-',
};

const defaultState: ITasksObject = {};
defaultState[PRIMARY_LANGUAGE] = [firstTask];

const tasks = (state: ITasksObject = defaultState, action: any) => {
  const tempState: ITasksObject = { ...state };
  const keys = Object.keys(tempState);

  keys.forEach((key) => {
    const keyedTasks: ITask[] = [...tempState[key]];
    tempState[key] = keyedTasks.map((item: ITask) => ({ ...item }));
  });

  switch (action.type) {
    case ADD_TASK: {
      const newTask = {
        ...defaultTask,
        index: action.newIndex,
      };

      keys.forEach((key) => {
        tempState[key].push(newTask);
      });

      return tempState;
    }
    case ADD_TASKS: {
      const { language } = action;
      const newLanguageTasks: ITask[] = action.tasks;
      tempState[language] = newLanguageTasks;
      return tempState;
    }
    case REMOVE_TASK: {
      keys.forEach((key) => {
        const filteredTasks: ITask[] = tempState[key].filter((task) => task.index !== action.index);
        tempState[key] = filteredTasks;
      });

      return tempState;
    }
    case CHANGE_TASK: {
      const { language, index } = action;
      if (!tempState[language]) {
        return state;
      }

      const languageTasks = tempState[language];
      if (languageTasks[index]) {
        languageTasks[index] = action.task;
      }
      return tempState;
    }
    case CHANGE_TASKS_LANGUAGE: {
      const { languageCode, oldLanguageCode } = action;

      const oldTasks: ITask[] = [...tempState[oldLanguageCode]];
      tempState[languageCode] = oldTasks;

      delete tempState[oldLanguageCode];

      return tempState;
    }
    case REMOVE_TASKS_LANGUAGE: {
      const { languageCode } = action;

      delete tempState[languageCode];

      return tempState;
    }
    case SET_TASKS:
      return action.tasks;
    default:
      return state;
  }
};

export default tasks;
