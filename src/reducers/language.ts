import {
  CHANGE_LANGUAGE_NAME, SET_LANGUAGE, REMOVE_LANGUAGE, ADD_NEW_LANGUAGE,
  PRIMARY_LANGUAGE, SET_ALL_LANGUAGES,
} from '../constants';

export type TLanguage = {
  current: string;
  languages: string[];
};

const defaultState: TLanguage = {
  current: PRIMARY_LANGUAGE,
  languages: [PRIMARY_LANGUAGE],
};

const language = (state: TLanguage = defaultState, action: any) => {
  switch (action.type) {
    case CHANGE_LANGUAGE_NAME: {
      const { languageCode, oldLanguageCode } = action;
      if (!languageCode || !oldLanguageCode) {
        return state;
      }

      const languages = [...state.languages];
      const index = languages.indexOf(oldLanguageCode);

      if (index > -1) {
        languages[index] = languageCode;
        return { ...state, current: languageCode, languages };
      }

      return state;
    }
    case SET_LANGUAGE: {
      const { languageCode } = action;
      let { current } = state;
      if (languageCode) {
        current = languageCode;
      }
      return { ...state, current };
    }
    case REMOVE_LANGUAGE: {
      const { languageCode } = action;
      const languages = [...state.languages].filter((lang) => lang !== languageCode);
      if (!languages.length) {
        return state;
      }

      return { ...state, current: languages[0], languages };
    }
    case ADD_NEW_LANGUAGE: {
      const { languageCode } = action;
      const newLanguages = [...state.languages];
      if (languageCode) {
        newLanguages.push(languageCode);
      }
      return { ...state, languages: newLanguages };
    }
    case SET_ALL_LANGUAGES:
      return action.languageState;
    default:
      return state;
  }
};

export default language;
