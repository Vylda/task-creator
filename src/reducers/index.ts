import { combineReducers } from 'redux';
import tasks from './tasks';
import openedTaskIndex from './openedTaskIndex';
import language from './language';
import dialog from './dialog';

export default combineReducers({
  tasks,
  openedTaskIndex,
  language,
  dialog,
});
