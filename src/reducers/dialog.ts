import { IconName } from '@blueprintjs/core';
import { ReactNode } from 'react';
import { SET_DIALOG_OPEN, SET_DIALOG_DONE } from '../constants';

interface IDialog {
  dialogIsOpen: boolean;
  done: boolean;
  icon: IconName;
  title: string;
  body: ReactNode;
}
interface IActionChange {
  type: typeof SET_DIALOG_OPEN;
  dialog: IDialog;
}

interface IActionDone {
  type: typeof SET_DIALOG_DONE;
  done: boolean;
}

const defaultState: IDialog = {
  dialogIsOpen: false,
  done: false,
  icon: 'warning-sign',
  title: '',
  body: '',
};

type TAction = IActionChange | IActionDone;

const dialogState = (state: IDialog = defaultState, action: TAction) => {
  const { type } = action;
  switch (type) {
    case SET_DIALOG_OPEN: {
      const { dialog } = action as IActionChange;
      const {
        dialogIsOpen, done, icon, title, body,
      } = dialog;
      return {
        ...state, dialogIsOpen, done, icon, title, body,
      };
    }
    case SET_DIALOG_DONE: {
      const { done } = action as IActionDone;
      return { ...state, done };
    }
    default:
      return state;
  }
};

export default dialogState;
