import { CHANGE_OPEN_TASK_INDEX } from '../constants';

const defaultState: number = 0;

const openedTaskIndex = (state: any = defaultState, action: any) => {
  switch (action.type) {
    case CHANGE_OPEN_TASK_INDEX:
      if (state === action.index) {
        return -1;
      }
      return action.index;
    default:
      return state;
  }
};

export default openedTaskIndex;
